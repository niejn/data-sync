#-*- coding:utf-8 -*-

import datetime
import pandas as pd

def extract_ac_keyword(path = "./d.txt"):
    # with open(path,'r', encoding='utf-8') as file:
    #     ans = file.readlines()
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
    # [line for line in res if line.strip()]
    clean_res = [line.strip() for line in res if line.strip()]
    return


def extract_keyword(path = "./sql_derive.txt"):
    # with open(path,'r', encoding='utf-8') as file:
    #     ans = file.readlines()
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
    # [line for line in res if line.strip()]
    clean_res = [line.strip() for line in res if line.strip()]
    return


def readtxt(path = "./test.txt"):
    # path = "./c.txt"
    res = []
    # coding=gbk
    # print(open(path).read())

    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
        print(res)
    # strlist = [x.strip().lower() for x in res[0].split('\t')]
    strlist = [x.split()[0].strip().lower() for x in res]

    print_headers(strlist, bar='\n')
    print_headers(strlist)
    print_headers(strlist, tag=':new.')
    format_header = ''
    for header in strlist:
        format_header += header +   ', '
        # print(header +  ',')
    print(format_header)
    print(strlist)
    print()

    # ans = ""
    # for a_str in strlist:
    #     a_str = "'" + a_str
    return


def print_headers(strlist, tag='', bar=', '):
    format_header = ''
    for header in strlist:
        format_header += tag + header + bar

        # print(header +  ',')
    print(format_header)


def main():
    # print(datetime.datetime.now())
    # d = datetime.datetime.now()
    # # d = datetime.datetime.fromtimestamp(timeStamp)
    # str1 = d.strftime("%H:%M:%S.%f")
    # header = "MDStreamID	SecurityID	 TotalLongPosition TradeVolume TotalValueTraded	PreSettlPrice" \
    #          "OpenPrice AuctionPrice 	AuctionQty 	HighPrice	LowPrice	TradePrice 	BuyPrice1	" \
    #          "BuyVolume1	SellPrice1	 SellVolume1	BuyPrice2	BuyVolume2 SellPrice2 SellVolume2	" \
    #          "BuyPrice3 BuyVolume3 SellPrice3	SellVolume3 BuyPrice4 BuyVolume4 " \
    #          "SellPrice4 SellVolume4 BuyPrice5	 BuyVolume5 	SellPrice5 	SellVolume5 SettlPrice " \
    #          "TradingPhaseCode	Timestamp"
    # print("testing")
    # df = pd.read_csv("../excel/test.csv", header=None, encoding='gb2312')
    # print(df)
    # extract_ac_keyword()
    # extract_keyword()
    readtxt()
    return



if __name__ == "__main__":
    main()