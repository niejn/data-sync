#-*- coding:utf-8 -*-

import datetime
import pandas as pd
import re
import time
import os
# from translate_v1 import
from helper.translate_v1 import translate
# from translate_v1 import translate

# futureID = re.compile(u'[a-zA-Z]+');
# multiplier = re.compile(u'\d+');
# for item in textlist:
#     id = futureID.findall(item)
#     mul = multiplier.findall(item)

def help_gen_type():
    data = pd.read_excel('./中证资本-仓单.xlsx', 'Sheet1', header=None)

    ori_db_keys = get_dbkeys('./d.txt')
    db_keys = {}
    db_types = {}
    data = data.iloc[:80, 0:3]

    ans = data.to_records(index=False)

    for a_record in ans:
        key = a_record[0]
        t_type = a_record[2]
        if key in ori_db_keys:
            db_keys[key] = ori_db_keys[key]
            # key_to_type[ori_db_keys[key]] = t_type
    return
def read_all(path):
    files = os.listdir(path)
    xls_list = []
    for file in files:
        file = path + '/' + file
        if not os.path.isfile(file):
            continue
        if file.endswith('xlsx'):
            xls_list.append(file)
            # srcFile.close()

    return xls_list

def gen_type(path=None):
    table_map = {'中证资本-仓单':'G_MANAGEMENT_WARRANT', '中证资本-场内股票':'G_MARKET_SHARES',
                 '中证资本-场外衍生':'G_OTC_DERIVATIVES', '中证资本-基差-点价':'G_BASIS_PRICING',
                 '中证资本-基差-期权':'G_BASIS_OPTION', '中证资本-期权做市':'TABLE G_OPTION_DEALER'}
    # path = './中证资本-仓单.xlsx'
    xls_name = os.path.splitext(os.path.split(path)[-1])[0]
    sheetname = 0
    # data = pd.read_excel(path, 'Sheet1',header=None)
    data = pd.read_excel(path, sheetname = 0,header=None)

    txt_name = xls_name + '.txt'
    db_table_name = table_map[xls_name]
    # data = data[:80]
    # iloc[1:5, 2:4]

    with open('./db_types.txt', 'r', encoding='utf-8', errors='ignore') as file:
        text = file.readlines()
    ori_db_types = {line.split()[0]:line.split(maxsplit=1)[1].strip(',\n') for line in text}
    with open("./type_map.txt", 'r', encoding='utf-8', errors='ignore') as file:
        text = file.readlines()
    type_map = {line.split('=')[0]:line.split('=')[1].strip() for line in text}
    ori_db_keys = get_dbkeys("./d.txt")
    db_keys = {}
    db_types = {}
    data = data.iloc[:80, 0:3]
    # print(data[2])
    # print(data)
    ans = data.to_records(index=False)


    key_to_type = {}
    for a_record in ans:
        # print(a_record)
        key = a_record[0]
        t_type = a_record[2]

        if key in ori_db_keys:
            db_keys[key] = ori_db_keys[key]
            key_to_type[ori_db_keys[key]] = t_type
        else:
            temp = translate(key)
            temp = temp.replace(" ", "_")
            db_keys[key] = temp.upper()
            key_to_type[temp.upper()] = t_type
            time.sleep(0.1)
    counter = {}
    for key in db_keys:
        counter[db_keys[key]] = 0
    for key in db_keys:
        if db_keys[key] in key_to_type:
            counter[db_keys[key]] += 1

    key_vals = list(db_keys.values())
    for key in key_to_type:
        if key in ori_db_types:
            key_to_type[key] = ori_db_types[key]
        else:
            if key_to_type[key] in type_map:
                key_to_type[key] = type_map[key_to_type[key]]
            else:
                print(key_to_type[key])
    # print(db_keys)sql_ddl.txt
    try:
        with open(txt_name, 'w') as file:
            str = 'CREATE TABLE {}\n'.format(db_table_name)
            file.write(str)
            str = '(\n'
            file.write(str)
            for key in key_to_type:
                str = '\t' + key + ' ' + key_to_type[key] + ',\n'
                file.write(str)
            str = ');\n'.format(db_table_name)
            file.write(str)
            for key in db_keys:
                str = "COMMENT ON COLUMN {}.{} IS '{}';\n".format(db_table_name, db_keys[key], key)
                file.write(str)
    except Exception as e:
        print(e)
    return



def get_dbkeys(path='./T_OWN_INVEST_DERIV.txt'):
    str = '权利金（若为买入期权）'
    ans = str.split("（")
    str = '持仓日期(数据日期)'
    ans = str.split("(")
    with open(path, 'r',encoding='utf-8', errors='ignore') as file:
        # res = file.read()
        res = file.readlines()
        # print(res)
    # COMMENT ON COLUMN T_OWN_INVEST_DERIV.REPORT_DATE IS '持仓日期(数据日期)';
    # res_tr = r"COMMENT ON COLUMN T_OWN_INVEST_DERIV.(.*?) IS '(.*?)'"
    res_tr = r"COMMENT ON COLUMN T_OWN_INVEST_DERIV.(.*) IS '(.*)'"
    urls = re.findall(res_tr, res[0], re.I | re.S | re.M)
    m_tr = re.findall(res_tr, res[0], re.S | re.M)
    keypairs = [re.findall(res_tr, key, re.S | re.M) for key in res if re.findall(res_tr, key, re.S | re.M)]
    # keydict = dict((apair[0], apair[1]) for apair in keypairs)
    # for apair in keypairs:
    #     print(apair[0][0])
    #     print(apair[0][1])
    # keydict = {apair[0][1].split("(")[0]:apair[0][0]  for apair in keypairs}
    keydict = {apair[0][1]: apair[0][0] for apair in keypairs}
    # path = './warrant_table.txt'
    # with open(path, 'r', encoding='utf-8', errors='ignore') as file:
    #     # res = file.read()
    #     res = file.readlines()
    #     print(res)

    long_key_pairs = [line.strip().split()[0] for line in res if line.strip().split()]
    # long_key_dict = {apair[1]: apair[0] for apair in long_key_pairs}
    index = 0
    # tkeys  = list(keydict.keys())

    # for key in long_key_pairs:
    #     if key in keydict:
    #         tkeys.remove(key)
    #         print(index)
    #         index += 1
    #         print(key)
    #         print(long_key_dict[key])
    #         print(keydict[key])
    #         long_key_dict[key] = keydict[key]
    # print(tkeys)
    # ans = {}
    # gap =  ''
    # keydict_keys = keydict.keys()
    # for key in long_key_pairs:
    #     if key in keydict_keys:
    #         ans[key] = keydict[key]
    #     else:
    #         temp = translate(key)
    #         temp = temp.replace(" ", "_")
    #         ans[key] = temp.upper()
    #         time.sleep(0.5)
    #     # print(index, gap, key, gap, ans[key])
    #     # index += 1
    #     print(ans[key])
    return keydict


def cr_comments(keys, notes):
    key_list = [key.strip().split() for key in keys.split('\n')]
    key_list_1 = [key[0] for key in key_list]
    note_list = [note.strip() for note in notes.split('\n')]
    for key, note in zip(key_list_1, note_list):
        ans = "COMMENT ON COLUMN G_OWN_INVEST_NEEQ.%s IS '%s';" % (key, note)

        print(ans)
        # match_list.append(ans)
    return


def match(keys, notes):
    # key_list = [key.strip().split() for key in keys.split('\n')]
    # key_list_1 = [key[0] for key in key_list]
    # # note_list = [
    # print(key_list)
    # print(key_list_1)
    keys_list = [key.strip() for key in keys.split('\n')]
    note_list = [note.strip() for note in notes.split('\n')]
    match_list = []
    # COMMENT ON TABLE G_OWN_INVEST_NEEQ IS '中证资本新三板业务数据接口表';
    # COMMENT ON COLUMN G_OWN_INVEST_NEEQ.REPORT_DATE IS '持仓日期(数据日期)';
    for key, note in zip(keys_list, note_list):
        ans = "%s--%s" % (key, note)

        # print(ans)
        match_list.append(ans)

    for key, note in zip(keys_list, note_list):
        ans = "%s--%s"%(key, note)

        # print(ans)
        match_list.append(ans)

    print(match_list)





    return







def main():
#     notes = '''持仓日期(数据日期)
# 业务类型
# 投资机构代码
# 证券代码
# 证券名称
# 发行人
# 交易市场
# 股票类别
# 初始投资金额
# 已退出金额
# 初始持股数量
# 已退出股数
# 持仓数量
# 持股比例
# 持仓成本
# 持仓市值
# 股票价格
# 三板成指价格
# 年初三板成指价格
# 市场日成交量
# 当日买入数量
# 当日买入价格
# 买入证券时的三板成指价格
# 当日卖出数量
# 当日卖出价格
# 币种
# 是否做市
# 具体转让方式
# 是否在限售期
# 是否在锁定期
# 是否已停牌
# 是否收益互换对冲券
# 是否两融融出证券
# 是否已冻结或被质押
# 已冻结或被质押金额（如有）
# 是否为集团内部交易
# 集团内部交易对手（如有）
# 集团内部交易金额（如有）
# 市盈率
# 市净率
# ROE
# 净利润同比增长率
# 其他需说明事项
# 数据更新时间'''
#     keys = '''REPORT_DATE             INTEGER,
#   BUSINESS_TYPE         VARCHAR2(64 BYTE),--业务类型
#   MANAGE_COMPANY_CODE     VARCHAR2(256 BYTE),
#   SECURITY_CODE           VARCHAR2(32 BYTE),
#   SECURITY_NAME           VARCHAR2(128 BYTE),
#   ISSUER_ENTITY           VARCHAR2(256 BYTE),
#   EXCHANGE_TYPE           VARCHAR2(16 BYTE),
#   STOCK_TYPE              VARCHAR2(16 BYTE),
#   PRINCIPLE_INVESTMENT    NUMBER(20,2),--
#   WITHDRAW_INVESTMENT     NUMBER(20,2),--
#   PRINCIPLE_HOLD_SHARE    NUMBER(20,2),--
#   WITHDRAW_SHARE          NUMBER(20,2),--
#   HOLD_AMOUNT             NUMBER(20,2),
#   HOLD_RATE               NUMBER(10,4),
#   HOLD_COST_VALUE         NUMBER(20,2),
#   HOLD_MARKET_VALUE       NUMBER(20,2),
#   CURRENT_NAV             NUMBER(20,2),
#   NEEQ_COMPONENT_INDEX    NUMBER(20,2),--
#   EARLIER_NEEQ_COMPONENT_INDEX        NUMBER(20,2),--
#   DAILY_TURNOVER          NUMBER(20,2),--
#   PURCHASE_QTY            NUMBER(20,2),--
#   PURCHASE_PRICE            NUMBER(20,2),--
#   NEEQ_COM_INDEX_W_PURCHASE           NUMBER(20,2),--
#   SELLING_QTY           NUMBER(20,2),--
#   SELLING_PRICE           NUMBER(20,2),--
#   CURRENCY_CODE           VARCHAR2(16 BYTE),
#   MARKET_MAKE_FLAG        VARCHAR2(1 BYTE),
#   TRANSFER_MODE           VARCHAR2(32 BYTE),
#   RESTR_TRADE_FLAG        VARCHAR2(1 BYTE),
#   LOCKUP_FLAG             VARCHAR2(1 BYTE),
#   SUSPEND_FLAG            VARCHAR2(1 BYTE),
#   SWAPS_HEDGE_FLAG        VARCHAR2(1 BYTE),
#   MARGIN_TRADE_FLAG       VARCHAR2(1 BYTE),
#   FREEZE_PLEDGE_FLAG      VARCHAR2(1 BYTE),
#   FREEZE_PLEDGE_VALUE     NUMBER(20,2),
#   GROUP_INTER_TRANS_FLAG  VARCHAR2(1 BYTE),
#   GROUP_COUNTER_PARTY     VARCHAR2(256 BYTE),
#   GROUP_COUNTER_VALUE     NUMBER(20,2),
#   PE_RATIO                NUMBER(20,2),--
#   PB_RATIO                NUMBER(20,2),--
#   ROE                     NUMBER(20,2),--
#   Net_profit_growth_rate               NUMBER(20,2),--
#   Notes                   VARCHAR2(256 BYTE),
#   LAST_MODIFIED_DATE      DATE'''
#
#     cr_comments(keys, notes)
#     match(keys, notes)
#     get_dbkeys()
#     help_gen_type()

    file_list = read_all('../requirements')
    for file in file_list:
        gen_type(file)
    return

if __name__ == "__main__":
    main()