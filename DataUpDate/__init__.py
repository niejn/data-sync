#-*- coding: utf-8 -*-

__all__ = ['mu1']

def demo():
    print("haha, testing in pkg1 init.py")
    return


# import pkg2.mu2
# from pkg2 import *
# __all__ = ['mu1']
#
# def demo():
#     print("haha, testing in pkg1 init.py")
#     return
# gl_test = "init 1 test"
#
# class test(object):
#     def f1(self):
#         global  gl_test
#         print(gl_test)
#         print('this is mod_one init - f1')
#
#         print('in mod_one-init-f1 > invoke mod_two-f2')
#         m_2 = mu2.mod_two()
#         m_2.f2()